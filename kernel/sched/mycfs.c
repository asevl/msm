#include "sched.h"
#include <linux/rbtree.h>

/*
 * mycfs scheduling class.
 *
 * implements a fair scheduling policy assuming equally-weighted processes
 * and ignoring nice values.
 *
 * can be pre-empted by by scheduler classes with a priority equal to or
 * greater than the sched_fair_class.
 *
 */

#define DEBUG_MYCFS_METHOD(x) printk(KERN_WARNING \
        "CPU%d====MYCFS====%s=========================\n", smp_processor_id(), x);

#define DEBUG_INT(label, x) printk(KERN_WARNING "              %s: %d", label, x);
#define DEBUG_LLU(label, x) printk(KERN_WARNING "              %s: %llu", label, x);
#define DEBUG_LU(label, x) printk(KERN_WARNING "              %s: %lu", label, x);
#define DEBUG_MSG(msg) printk(KERN_WARNING "              %s", msg);

unsigned int sysmycfs_sched_latency = 10000000ULL; /* schedule latency = 10ms */
/* unsigned int sysmcfs_sched_min_granularity = 0;   probably won't use this */

/* ============================================================================
 *
 * UTILITY METHODS
 *
 */

/*
 * uses container_of() macro to get the task_struct associated with
 * this entity
 */
static inline struct task_struct *task_of(struct sched_mycfs_entity *my_se)
{
    return container_of(my_se, struct task_struct, my_se);
}

/*
 * uses container_of() macro to get the cpu runqueue that contains
 * this MYCFS runqueue
 */
static inline struct rq *rq_of(struct mycfs_rq *my_rq)
{
    return container_of(my_rq, struct rq, mycfs);
}

/* ============================================================================
 *
 * VRUNTIME CALCULATIONS
 *
 */

static inline u64 max_vruntime(u64 min_vruntime, u64 vruntime)
{
    s64 diff = (s64)(vruntime - min_vruntime);
    if (diff > 0)
        min_vruntime = vruntime;

    return min_vruntime;
}

static inline u64 min_vruntime(u64 min_vruntime, u64 vruntime)
{
    s64 diff = (s64)(vruntime - min_vruntime);
    if (diff < 0)
        min_vruntime = vruntime;

    return min_vruntime;
}

/* returns > 0 if the first is larger than the second  */
static inline s64 compare_vruntimes(u64 v1, u64 v2)
{
    return (s64)(v1 - v2);
}

static unsigned long
sched_slice(struct mycfs_rq *my_rq, struct sched_mycfs_entity *this)
{
    unsigned long slice = (unsigned long) sysmycfs_sched_latency;
    if (nr_running > 0)
        slice = slice / (unsigned long) nr_running;
    return slice;
}

/* METHOD DECLARATIONS */

static unsigned int insert_sched_entity (struct mycfs_rq *rq, struct sched_mycfs_entity *this_my_se);
static int remove_sched_entity (struct mycfs_rq *rq, struct sched_mycfs_entity *del_this);


/* ============================================================================
 *
 * VRUNTIME UPDATING
 *
 * */

/* update_min_vruntime
 * ----------------------------------------------------------------------------
 * update the current minimum vruntime of the given MYCFS runqueue
 *
 * looks at:
 *
 *      + the vruntime of the current
 *      + the vruntime of the leftmost node
 *
 * Updates the minimum to:
 *
 *      max(current minimum, min(leftmost process, current process))
 */
static void
update_min_vruntime(struct mycfs_rq *my_rq)
{
    u64 this_vruntime = my_rq->min_vruntime;
    struct sched_mycfs_entity *leftmost = NULL;

    DEBUG_MYCFS_METHOD("START update_min_vruntime");
    DEBUG_LLU("old min_vruntime", this_vruntime);

    /* if there's a current MYCFS class process */
    /* set this to its vruntime */
    if (my_rq->curr) {
        DEBUG_MSG("lookint at current")
        this_vruntime = my_rq->curr->vruntime;
    }

    /* if there's a leftmost node on the runqueue */
    /* get the min(current process vruntime, leftmost process vruntime) */
    if (my_rq->rb_leftmost) {
        DEBUG_MSG("looking at leftmost");
        leftmost = rb_entry(my_rq->rb_leftmost, struct sched_mycfs_entity, run_node);

        if (!my_rq->curr)
            this_vruntime = leftmost->vruntime; /* if the current process already exited
                                                   we don't need to take the min anymore,
                                                   we know the min vruntime will be that
                                                   of the leftmost node */
        else
            this_vruntime = min_vruntime(this_vruntime, leftmost->vruntime);
    }

    /* update the minimum to the max(current minimum, min(leftmost process, current process)) */
    my_rq->min_vruntime = max_vruntime(my_rq->min_vruntime, this_vruntime);
    DEBUG_LLU("new min_vruntime", my_rq->min_vruntime);
    DEBUG_MYCFS_METHOD("END update_min_vruntime");
}

static void
update_curr(struct mycfs_rq *my_rq)
{
    struct sched_mycfs_entity *curr = my_rq->curr;
    u64 now = rq_of(my_rq)->clock_task;

    unsigned long exec_time;

    DEBUG_MYCFS_METHOD("START update_curr");
    DEBUG_INT("process: %d", task_of(curr)->pid);

    if (!curr)
        return;

    exec_time = (unsigned long)(now - curr->exec_start);
    if (!exec_time)
        return;

    DEBUG_LLU("exec_start", curr->exec_start);
    DEBUG_LLU("old vruntime", curr->vruntime);
    DEBUG_LU("exec_time", exec_time);
    curr->vruntime += exec_time;
    curr->schedlimit_exec_time += exec_time;

    DEBUG_LLU("curr's new vruntime", curr->vruntime);
    curr->exec_start = now;
    update_min_vruntime(my_rq);

    DEBUG_MYCFS_METHOD("END update_curr");
}

/* ============================================================================
 *
 * SCHEDULER HOOKS
 *
 *
 * */

#ifdef CONFIG_SMP
static int
select_task_rq_mycfs(struct task_struct *p, int sd_flag, int flags)
{
	return task_cpu(p); /* mycfs tasks never migrate */
}
#endif /* CONFIG_SMP */

static struct sched_mycfs_entity *
pick_min_entity(struct mycfs_rq *my_rq)
{
    struct sched_mycfs_entity *my_se = NULL;

    /* Get the leftmost node */
	struct rb_node *left = my_rq->rb_leftmost;

    DEBUG_MYCFS_METHOD("START pick_min_entity");

    /* If no leftmost, run queue is empty */
    if (!left) {
        printk(KERN_WARNING "    ========== err: no leftmost cached\n");
		return NULL;
    }

    printk(KERN_WARNING "     ========== getting sched entity\n");

    /* Get sched entity associated with node */
	my_se = rb_entry(left, struct sched_mycfs_entity, run_node);
    DEBUG_MYCFS_METHOD("END pick_min_entity");

    return my_se;
}

/* pick_next_task_mycfs
 * ----------------------------------------------------------------------------
 * Chooses most appropriate task to run next.
 *
 * In this case, that would be the task with the lowest current vruntime.
 *
 */
static struct task_struct *
pick_next_task_mycfs(struct rq *rq)
{
    /* struct task_struct *p;*/
    struct mycfs_rq *mycfs_rq = &rq->mycfs;
    struct sched_mycfs_entity *my_se;

    DEBUG_MYCFS_METHOD("START pick_next_task");

    /* Check that there are processes on the running queue */
    if (mycfs_rq->nr_running < 0) {
        printk(KERN_WARNING "    ========== err: none running\n");
        return NULL;
    } else {
        printk(KERN_WARNING "     ========== nr_running: %lu\n", mycfs_rq->nr_running);
    }

    my_se = pick_min_entity(mycfs_rq);
    printk(KERN_WARNING "     =========== picked: %d\tvruntime: %llu\n", task_of(my_se)->pid, my_se->vruntime);

    if(my_se->on_rq)
        remove_sched_entity(mycfs_rq, my_se);

    my_se->exec_start = rq_of(mycfs_rq)->clock_task;

    mycfs_rq->curr = my_se;
    DEBUG_INT("new mycfs curr", task_of(my_se)->pid);

    DEBUG_MYCFS_METHOD("END pick_next_task");
    /* Return task_struct of that sched entity */
    return task_of(my_se);
}

/* check_preempt_curr_mycfs
 * ----------------------------------------------------------------------------
 * Checks if a newly runnable process has a lower vruntime than the currently
 * running process; the one with the lowest vruntime is chosen to run.
 *
 * Usually called when a task is enqueued or dequeued.
 *
 * PREEMPT mycfs runqueue is not empty AND curr is not a SCHED_MYCFS task
 * PREEMPT curr is SCHED_MYCFS task AND does not have the smalled vruntime
 *
 * */
static void
check_preempt_curr_mycfs(struct rq *rq, struct task_struct *p, int flags)
{

    struct task_struct *curr = rq->curr;
    struct sched_mycfs_entity *curr_ent = &curr->my_se;
    struct sched_mycfs_entity *p_ent = &p->my_se;
    struct mycfs_rq *curr_rq = &task_rq(p)->mycfs;

    DEBUG_MYCFS_METHOD("START check_preempt_curr");

    /* improbable, but if a process prempted itself, stop */
    if (curr_ent == p_ent)
        return;

    /* update the runqueue */
    update_curr(curr_rq);

    /* compare vruntimes, if newly runnable process has a smaller
     * vruntime than the current process , reschedule the current process
     */
    if (compare_vruntimes(curr_ent->vruntime, p_ent->vruntime) < 0)
        resched_task(curr);

    DEBUG_MYCFS_METHOD("END check_preempt_curr");
}


/* enqueue_task_mycfs
 * ----------------------------------------------------------------------------
 * called when a SCHED_MYCFS task becomes runnable
 *
 *
 *
 */
static void
enqueue_task_mycfs(struct rq *rq, struct task_struct *p, int flags)
{
    /* TODO */
    struct mycfs_rq *my_rq = &rq->mycfs;
    struct sched_mycfs_entity *my_se = &p->my_se;
    u64 vruntime = my_rq->min_vruntime;

    DEBUG_MYCFS_METHOD("START enqueue_task");
    DEBUG_LU("nr_running", my_rq->nr_running);

    update_curr(my_rq);

    /* set vruntime of enqueued process to at least the current
     * minimum, otherwise we would lose track of it
     * */
    vruntime = max_vruntime(my_se->vruntime, vruntime);
    my_se->vruntime = vruntime;

    /* new runnable mycfs process */
    my_rq->nr_running++;

    /* if the process is not the current process (i.e., not on the runqueue)
     * then insert it */
    if(my_se != my_rq->curr) {
        insert_sched_entity(my_rq, my_se);
    }
    /* process now on the mycfs runqueue */
    my_se->on_rq = 1;

    /* if process dead, increment num current processes running */
    if (!my_se)
	    inc_nr_running(rq);

    DEBUG_LU("new nr_running", my_rq->nr_running);
    DEBUG_MYCFS_METHOD("END enqueue task");
}

/* dequeue_task_mycfs
 * ----------------------------------------------------------------------------
 * called when a SCHED_MYCFS task is no longer runnable
 *
 *
 */
static void
dequeue_task_mycfs(struct rq *rq, struct task_struct *p, int flags)
{
    /* TODO */
    struct mycfs_rq *my_rq = &rq->mycfs;
    struct sched_mycfs_entity *this_se = &p->my_se;

    DEBUG_MYCFS_METHOD("START dequeue_task")
    DEBUG_LU("nr_running", my_rq->nr_running);

    /* update run queue and vruntime */
    update_curr(my_rq);

    /* reverse enqueue
     *
     * check that the dequeued process is not the current process on the run
     * queue before taking it out of the tree.
     *
     * */
    if (this_se != my_rq->curr)
        remove_sched_entity(my_rq, this_se);
    this_se->on_rq = 0;

    /* decrement number of runnable processes in the tree */
    my_rq->nr_running--;

    /* update the minimum vruntime in the current run queue */
    update_min_vruntime(my_rq);

    /* if process dead, decrement num current processes running */
    if (!this_se)
	    dec_nr_running(rq);

    DEBUG_LU("new nr_running", my_rq->nr_running);
    DEBUG_MYCFS_METHOD("END dequeue_task")
}

static void
yield_task_mycfs(struct rq *rq)
{
    /* TODO */
    DEBUG_MYCFS_METHOD("START yield_task");

    /* if only one runnable task, don't continue */
    if (rq->nr_running == 1)
        return;

    /* note: should only update_curr()
     *
     * rescheduling will be handled by schedule()
     *
     * */
    update_rq_clock(rq);
    update_curr(&rq->mycfs);
    rq->skip_clock_update = 1;
    DEBUG_MYCFS_METHOD("END yield_task");
}

static void
put_prev_task_mycfs(struct rq *rq, struct task_struct *prev)
{
    struct mycfs_rq *my_rq = &rq->mycfs;
    struct sched_mycfs_entity *prev_se = &prev->my_se;

    DEBUG_MYCFS_METHOD("START put_prev_task");
    if(prev_se->on_rq)
        update_curr(my_rq);
    if(prev_se->on_rq)
        insert_sched_entity(my_rq, prev_se);

    my_rq->curr = NULL;

    DEBUG_MYCFS_METHOD("END put_prev_task");
}

static void
task_tick_mycfs(struct rq *rq, struct task_struct *curr, int queued)
{
    /* TODO */
    unsigned long diff;
    struct mycfs_rq *my_rq = &rq->mycfs;
    struct sched_mycfs_entity *this_curr = &curr->my_se;
    struct sched_mycfs_entity *new_curr = NULL;
    unsigned long slice = sched_slice(my_rq, this_curr);
    DEBUG_MYCFS_METHOD("START task_tick");


    /* update the current process vruntime */
    update_curr(my_rq);

    /* if there are other runnable processes, check to see if we should
     * prempt this one
     * */
    if (my_rq->nr_running > 1) {

        new_curr = pick_min_entity(my_rq);
        diff = compare_vruntimes(this_curr->vruntime, new_curr->vruntime);

        if (diff > slice)
            resched_task(rq_of(my_rq)->curr);

    }

    DEBUG_MYCFS_METHOD("END task_tick");
}

/* set_curr_task_mycfs
 * ----------------------------------------------------------------------------
 * called when a task changes its scheduling class
 *
 * sets the current task (current) policy to mycfs and adds it to the
 * runqueue.
 *
 * */
static void
set_curr_task_mycfs(struct rq *rq)
{
    /* TODO */
    struct sched_mycfs_entity *this_se = &rq->curr->my_se;
    struct mycfs_rq *my_rq = &rq->mycfs;

    DEBUG_MYCFS_METHOD("START set_curr_task");
    DEBUG_INT("curr task", rq->curr->pid);

    if (this_se->on_rq)
        remove_sched_entity(my_rq, this_se);

    /* picking new current task */
    this_se->exec_start = rq_of(my_rq)->clock_task;
    my_rq->curr = this_se;
    DEBUG_MYCFS_METHOD("END set_curr_task");
}

/* switched_to_mycfs
 * ----------------------------------------------------------------------------
 * called when a task switches to a mycfs_sched_class task
 *
 */
static void
switched_to_mycfs(struct rq *rq, struct task_struct *p)
{
    /* TODO */
    DEBUG_MYCFS_METHOD("START switched_to");
    DEBUG_INT("rq curr", rq->curr->pid);
    DEBUG_INT("this curr", p->pid);
    // resched_task(rq->curr);

    if (!p->my_se.on_rq)
        return;

    /* should clear the cpu limit of processes which have switches to mycfs */
    p->sched_mycfs_limit = 0;

    /* if the current process is the process passed */
    if (rq->curr == p) {
        resched_task(rq->curr);
    } else {
        check_preempt_curr(rq, p, 0);
    }
    DEBUG_MYCFS_METHOD("END switched_to");
}

static unsigned int
get_rr_interval_mycfs(struct rq *rq, struct task_struct *task)
{
    struct sched_mycfs_entity *this_se = &task->my_se;
    struct mycfs_rq *my_rq = &rq->mycfs;
    unsigned long interval = sched_slice(my_rq, this_se);

    DEBUG_MYCFS_METHOD("get_rr_interval");
    DEBUG_LU("nr_running", my_rq->nr_running);

    return NS_TO_JIFFIES(interval);
}

static void
task_fork_mycfs(struct task_struct *p)
{
    /* TODO */
    struct mycfs_rq *my_rq;
    struct sched_mycfs_entity *this_se = &p->my_se;
    struct sched_mycfs_entity *curr_se = NULL;
    struct rq *rq = this_rq();
    int this_cpu = smp_processor_id();
    unsigned long flags;
    u64 new_vruntime;

    DEBUG_MYCFS_METHOD("START task_fork");
    DEBUG_INT("child task", p->pid);

    /* lock cpu runqueue lock */
    raw_spin_lock_irqsave(&rq->lock, flags);

    update_rq_clock(rq); /* call clock update on the rq, we have lock */
    my_rq = &task_rq(current)->mycfs;
    curr_se = my_rq->curr;

    /* if the cpu on which the child task is executing is NOT the cpu
     * which is currently excuting, move the child task over
     */
    if(task_cpu(p) != this_cpu) {
        rcu_read_lock();
        __set_task_cpu(p, this_cpu);
        rcu_read_unlock();
    }

    update_curr(my_rq);

    /* if mycfs has a current process, set the child's vruntime
     * to this vruntime
     */
    if (curr_se)
        this_se->vruntime = curr_se->vruntime;

    /* make sure we account for new task vruntime */
    new_vruntime = my_rq->min_vruntime;
    new_vruntime = max_vruntime(this_se->vruntime, new_vruntime);
    this_se->vruntime = new_vruntime;

    /* unlock cpu runqueue lock */
    raw_spin_unlock_irqrestore(&rq->lock, flags);

    DEBUG_MYCFS_METHOD("END task_fork");
}


/*
 * insert_sched_entity:
 *
 * Insert a sched_mycfs_entity into the rbtree of a given mycfs_rq running
 * queue. Begins at root node, comparing the vruntime of the sched
 * entity with each next child until we reach a null child. We insert
 * the sched entity's run_node here.
 *
 * Return 0 to indicate success.
 *
 */
static unsigned int
insert_sched_entity (struct mycfs_rq *rq, struct sched_mycfs_entity *new)
{
	struct rb_node **link = &rq->root_node.rb_node; //root of tree
	struct rb_node *parent = NULL;
    struct sched_mycfs_entity *next_child = NULL;
    struct sched_mycfs_entity *leftmost = NULL;
    s64 cmp_vruntime;
    int link_null = *link ? 1 : 0;

    DEBUG_MYCFS_METHOD("insert_sched_entity");
    DEBUG_INT("inserting", task_of(new)->pid);
    DEBUG_INT("first link exists", link_null);

    /* While there are nodes left to compare */
	while(*link) {

		parent = *link;

		/* Get sched entity of next child */
        next_child = rb_entry(parent, struct sched_mycfs_entity, run_node);
        if (!next_child) {
            DEBUG_MSG("err: next child was null");
            break;
        }
        DEBUG_INT("looking at child", task_of(next_child)->pid);
        cmp_vruntime = compare_vruntimes(new->vruntime, next_child->vruntime);
        printk(KERN_WARNING "=========== comparing %llu and %llu: %lld\n", new->vruntime, next_child->vruntime, cmp_vruntime);

        /*
         * Traverse the rbtree:
         *
         * If the vruntime value of the to-be-inserted sched entity
         * is greater than the child, we move right.
         * Otherwise we move left.
         */
		if(cmp_vruntime > 0) {
			link = &parent->rb_right;
            DEBUG_MSG("move right");
		} else {
            link = &parent->rb_left;
            DEBUG_MSG("move left");
		}
	}

    leftmost = rb_entry(rq->rb_leftmost, struct sched_mycfs_entity, run_node);
    if (!leftmost || compare_vruntimes(new->vruntime, leftmost->vruntime) < 0) {
        rq->rb_leftmost = &new->run_node;
        DEBUG_LLU("new leftmost", new->vruntime);
    }

	rb_link_node(&new->run_node, parent, link);
	rb_insert_color(&new->run_node, &rq->root_node);
	return 0;
}


/*
 * remove_sched_entity
 * ----------------------------------------------------------------------------
 *
 */
static int
remove_sched_entity (struct mycfs_rq *rq, struct sched_mycfs_entity *del_this)
{
    struct rb_node *next = NULL;

    DEBUG_MYCFS_METHOD("remove_sched_entity");
    DEBUG_INT("removing", task_of(del_this)->pid);

    /* update leftmost node if node being deleted is the current leftmost */
    if (rq->rb_leftmost == &del_this->run_node) {
        next = rb_next(&del_this->run_node);
        rq->rb_leftmost = next;
    }

    rb_erase(&del_this->run_node, &rq->root_node);
    return 0;
}

/*
 * init_mycfs_rq
 * ----------------------------------------------------------------------------
 *
 * initializes the runqueue used by the mycfs_sched_class with an rbtree
 * and a min_vruntime value calculation taken from the linux fair scheduling
 * class.
 *
 */

void
init_mycfs_rq(struct mycfs_rq *mycfs_rq)
{
    mycfs_rq->root_node = RB_ROOT;
    mycfs_rq->min_vruntime = (u64)(-(1LL << 20));
    DEBUG_MYCFS_METHOD("init_mycfs_rq");
}


/*
 * Simple fair scheduling class
 */
const struct sched_class mycfs_sched_class = {
	.next			    = &idle_sched_class,

	.enqueue_task		= enqueue_task_mycfs,
	.dequeue_task		= dequeue_task_mycfs,
	.yield_task		    = yield_task_mycfs,

	.check_preempt_curr	= check_preempt_curr_mycfs,

	.pick_next_task		= pick_next_task_mycfs,
	.put_prev_task		= put_prev_task_mycfs,

#ifdef CONFIG_SMP
	.select_task_rq		= select_task_rq_mycfs,
#endif

	.set_curr_task      = set_curr_task_mycfs,
	.task_tick	    	= task_tick_mycfs,

	.get_rr_interval	= get_rr_interval_mycfs,

	.switched_to		= switched_to_mycfs,

    .task_fork          = task_fork_mycfs
};
