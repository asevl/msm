#include <linux/slab.h>
#include <linux/errno.h>
#include <linux/linkage.h>

/*
 * system call: sys_ext4_cowcopy
 * ----------------------------------------------------------------------------
 *
 *  new sytem call to support the COW mechanism for file copying in the
 *  ext4 fileystem.
 *
 */
asmlinkage int sys_ext4_cowcopy(const char __user *src, const char __user *dest)
{
    //unsigned long len = PATH_MAX;
    //const char *src_filename;
    //const char *dest_filename;

    //copy_from_user(src_filename, src, strlen(src));

    /* TODO: src should be a regular file and cannot be a directory */
    if (0) {
        return -EPERM;
    }
    /* TODO: src must be a file in the ext4 filesystem */
    if (0) {
        //return -ENOTSUP;
    }

    /* TODO: src and dest should be in the same device */
    if (0) {
        return -EXDEV;
    }

    return 0;
}
/*
 * fs_is_target
 * -----------------------------------------------------------------------
 *
 * sb: takes a super_block struct
 * target_fs_name: the name of the file system type, e.g., ext4
 *
 * returns 1 if the super_block file system type is the target
 *           file system type specified by the parameter
 *
 * returns 0 if the file system types did not match
 *
 */
static int fs_is_target(struct super_block *sb, const char* target_fs_name)
{
    int result;
    const char *sb_fs_name = sb->s_type->name;

    while (*sb_fs_name && (*sb_fs_name == *target_fs_name))
        sb_fs_name++, target_fs_name++;

    result = *(const unsigned char *)sb_fs_name -
                            *(const unsigned char *)target_fs_name;

    return (result == 0 ? 1 : 0);
}
