#include <linux/types.h>
#include <linux/signal.h>
#include <linux/errno.h>
#include <linux/sched.h>
#include <linux/timer.h>
#include <linux/pid.h>

asmlinkage int sys_sched_setlimit(pid_t pid, int limit)
{
    int ret = -1;

    /* limit must be between 0 and 100 */
    if (limit < 0 || limit > 100) {
        return -EINVAL;
    /* process must exist */
    } else if ((int) pid < 0) {
        return -EINVAL;
    } else {
        struct pid *this_pid = find_vpid((int)pid);
        struct task_struct *this_task = get_pid_task(this_pid, PIDTYPE_PID);
        printk(KERN_WARNING "sys_sched_setlimit pid: %d \tlimit:%d", (int)pid, limit);



        /* if no process, good process wasn't supplied */
        if (!this_task) {
            return -EINVAL;
        }

        /* then we're clearing the cpu usage limit */
        if (limit == 0) {
            this_task->sched_mycfs_limit = 0;
            this_task->my_se.schedlimit_exec_time = 0;
            /* need to clear the timer */
        } else {

            /* set new usage limit */
            this_task->sched_mycfs_limit = limit;

            /* reset the usage/exec time */
            this_task->my_se.schedlimit_exec_time = 0;

            /* initialize and set the timer */
            init_timer(&(this_task->my_se.schedlimit_timer));

        }

        ret = 0;
    }
    return ret;
}
